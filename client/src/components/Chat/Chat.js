import React, { useState, useEffect } from 'react'
import queryString from 'query-string'
import io from 'socket.io-client'

let socket;

// Make a destructuring on the location props
const Chat = ({ location }) => {
    const [name, setName] = useState('')
    const [room, setRoom] = useState('')
    const ENDPOINT = 'localhost:5000'

    useEffect(() => {
        // location.search return de query parameters
        // The queryString parse the query parameters to a object
        // With this, we can use for example data.name that return the name
        // const {data} = queryString.parse(location.search)

        // Make a destructuring with the queryString
        const { name, room } = queryString.parse(location.search)

        // Passing the endpoint
        socket = io(ENDPOINT);

        setName(name)
        setRoom(room)

        // The first parameter of emit is the string what the backend will be recognize
        socket.emit('join', { name, room })

    }, [ENDPOINT, location.search])


    return (
        <h1>Chat</h1>
    )
}

export default Chat